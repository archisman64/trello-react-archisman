export const checklistsReducer = (state, action) => {
    switch (action.type) {
        case 'ADD_CHECKLIST':
            return { ...state, data: [...state.data, action.payload] };
        case 'DELETE_CHECKLIST':
            // console.log('to delte cheklist id:', action.payload);
            return { ...state, data: state.data.filter((list) => list.id !== action.payload) };
        // return state.filter((list) => list.id !== action.payload);
        case 'SET_CHECKLISTS':
            return { ...state, data: action.payload };
        // return action.payload;
        case 'TOGGLE_CARD_DELETE_CONFIRMATION':
            return { ...state, isDeleteConfirmationOpen: !state.isDeleteConfirmationOpen };
        case 'TOGGLE_CHECKLIST_MODAL_STATUS':
            return { ...state, isChecklistModalOpen: !state.isChecklistModalOpen };
        case 'TOGGLE_ADDING_CHECKLIST':
            return { ...state, isAddingChecklist: !state.isAddingChecklist };
        case 'SET_NEW_CHECKLIST_NAME':
            return { ...state, newChecklistName: action.payload };
        default:
            return state;
    }
};
