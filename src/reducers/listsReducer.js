export const listsReducer = (state, action) => {
    switch (action.type) {
        case 'ADD_LIST':
            return [...state, action.payload];
        case 'DELETE_LIST':
            return state.filter((list) => list.id !== action.payload);
        case 'SET_LISTS':
            return action.payload;
        default:
            return state;
    }
};
