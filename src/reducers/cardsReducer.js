export const cardsReducer = (state, action) => {
    switch (action.type) {
        case 'ADD_CARD':
            return [...state, action.payload];
        case 'DELETE_CARD':
            return state.filter((card) => card.id !== action.payload);
        case 'SET_CARDS':
            return action.payload;
        default:
            return state;
    }
};