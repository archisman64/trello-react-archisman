export const checkItemsReducer = (state, action) => {
    switch (action.type) {
        case 'ADD_CHECKITEM':
            return { ...state, data: [...state.data, action.payload] };
        case 'DELETE_CHECKITEM':
            return { ...state, data: state.data.filter((list) => list.id !== action.payload) };
        case 'TOGGLE_CHECKED_STATE':
            return {
                ...state,
                data: state.data.map((item) => item.id === action.payload.checkItemId ? { ...item, state: action.payload.newState } : item)
            }
        case 'SET_CHECKITEMS':
            return { ...state, data: action.payload };
        case 'TOGGLE_SHOW_INPUT':
            return { ...state, showInput: !state.showInput };
        case 'SET_CHECKITEM_NAME':
            return { ...state, newCheckItemName: action.payload };
        default:
            return state;
    }
};
