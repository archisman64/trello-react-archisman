import axios from 'axios';

const API_KEY = '78c5f59f0a43a9a4791ae8d412d09ca9';
const ACCESS_TOKEN = 'ATTAf7fdb572e67e2808d7704f0660a5ae9a954872b045254c483d83f50e2d03052067A3E0DB';

const baseURL = 'https://api.trello.com/1';

const trelloApi = axios.create({
    baseURL,
    params: {
        key: API_KEY,
        token: ACCESS_TOKEN,
    },
});

// Function to get boards
export const getBoards = async () => {
    try {
        const response = await trelloApi.get('/members/me/boards');
        return response.data;
    } catch (error) {
        console.error('Error fetching boards:', error);
        throw error;
    }
};

// Function to get lists for a board
export const getListsForBoard = async (boardId) => {
    try {
        const response = await trelloApi.get(`/boards/${boardId}/lists`);
        return response.data;
    } catch (error) {
        console.error('Error fetching lists:', error);
        throw error;
    }
};

// Function to get cards for a list
export const getCardsForList = async (listId) => {
    try {
        const response = await trelloApi.get(`/lists/${listId}/cards`);
        return response.data;
    } catch (error) {
        console.error('Error fetching cards:', error);
        throw error;
    }
};

export const createBoard = async (boardName) => {
    try {
        const response = await trelloApi.post('boards', {
            name: boardName,
            defaultLabels: false,
            defaultLists: false,
        });
        return response.data;
    } catch (error) {
        console.error('Error creating board:', error);
        throw error;
    }
};

export const createCard = async (listId, cardName) => {
    try {
        const response = await trelloApi.post(`/cards`, {
            idList: listId,
            name: cardName,
        });
        return response.data;
    } catch (error) {
        console.error('Error creating card:', error);
        throw error;
    }
};

export const createList = async (boardId, listName) => {
    try {
        const response = await trelloApi.post(`/lists`, {
            idBoard: boardId,
            name: listName,
        });
        return response.data;
    } catch (error) {
        console.error('Error creating list:', error);
        throw error;
    }
};

export const deleteList = async (listId) => {
    try {
        const response = await trelloApi.put(`/lists/${listId}?closed=true`);
        return response.data;
    } catch (error) {
        console.error('Error deleting list:', error);
        throw error;
    }
};

export const deleteCard = async (cardId) => {
    try {
        console.log('card id in service:', cardId)
        const response = await trelloApi.delete(`/cards/${cardId}`);
        return response.data;
    } catch (error) {
        console.error('Error deleting card:', error);
        throw error;
    }
};

export const getChecklistsForCard = async (cardId) => {
    try {
        const response = await trelloApi.get(`/cards/${cardId}/checklists`);
        return response.data;
    } catch (error) {
        console.error('Error fetching checklists:', error);
        throw error;
    }
};

export const addChecklistToCard = async (cardId, checklistName) => {
    try {
        const response = await trelloApi.post(`/cards/${cardId}/checklists?name=${checklistName}`);
        return response.data;
    } catch (error) {
        console.error('Error fetching checklists:', error);
        throw error;
    }
};

export const deleteChecklistFromCard = async (cardId, checklistId) => {
    try {
        const response = await trelloApi.delete(`/cards/${cardId}/checklists/${checklistId}`);
        return response.data;
    } catch (error) {
        console.error('Error fetching checklists:', error);
        throw error;
    }
};

export const getCheckItemsForChecklist = async (checklistId) => {
    try {
        const response = await trelloApi.get(`/checklists/${checklistId}/checkItems`);
        return response.data;
    } catch (error) {
        console.error('Error fetching checkItems:', error);
        throw error;
    }
};

export const addCheckItemToChecklist = async (checklistId, newCheckItemName) => {
    try {
        const response = await trelloApi.post(`/checklists/${checklistId}/checkItems?name=${newCheckItemName}`);
        return response.data;
    } catch (error) {
        console.error('Error adding checkItem:', error);
        throw error;
    }
};

export const deleteCheckItemFromChecklist = async (checklistId, checkItemId) => {
    try {
        const response = await trelloApi.delete(`/checklists/${checklistId}/checkItems/${checkItemId}`);
        return response.data;
    } catch (error) {
        console.error('Error deleting checkItem:', error);
        throw error;
    }
};

export const updateCheckItem = async (cardId, checkItemId, chekItemStatus) => {
    try {
        const response = await trelloApi.put(`/cards/${cardId}/checkItem/${checkItemId}?state=${chekItemStatus}`);
        return response.data;
    } catch (error) {
        console.error('Error updating checkItem:', error);
        throw error;
    }
};