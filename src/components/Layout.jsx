// import React from 'react';
import TopBar from './TopBar';
import BoardsComponent from './Boards';
import { Routes, Route } from 'react-router-dom';
import Lists from './Lists';
import { getBoards } from '../services/trelloApi';
import { useState, useEffect } from 'react';
import Loader from './Loader';
import ErrorComponent from './ErrorComponent';
import './layout.css';


const Layout = () => {
    const [error, setError] = useState(null);
    const [boards, setBoards] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        getBoards()
            .then((boardsData) => {
                setBoards(boardsData);
                setIsLoading(false);
            })
            .catch((error) => {
                console.error('Error fetching boards:', error)
                setError(error)
            });
    }, []);

    return (
        <div className='layout'>{error ? <ErrorComponent error={error} /> :
            <>
                <Routes>
                    <Route path='/' element={
                        <>
                            <TopBar />
                            {isLoading ? <Loader /> :
                                <BoardsComponent boards={boards} setBoards={setBoards} />}
                        </>
                    } />
                    <Route path='/boards/:boardId' element={
                        <>
                            <TopBar />
                            <Lists boards={boards} />
                        </>
                    } />
                    <Route path='/*' element={
                        <ErrorComponent error={{ 'statusCode': 404, 'message': 'page not found.' }} />
                    } />
                </Routes>
            </>}
        </div>
    );
};

export default Layout;