// import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Container from '@mui/material/Container';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';

const TopBar = () => {
    return (
        <AppBar position="sticky">
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <Typography
                        variant="h6"
                        noWrap
                        component={Link}
                        to="/"
                        sx={{
                            mr: 2,
                            fontFamily: 'monospace',
                            fontWeight: 700,
                            letterSpacing: '.3rem',
                            color: 'inherit',
                            textDecoration: 'none',
                        }}
                    >
                        Trello
                    </Typography>
                    <Link to="/" style={{ textDecoration: 'none' }}>
                        <Typography
                            variant="body1"
                            noWrap
                            sx={{
                                mr: 2,
                                color: 'white',
                                textDecoration: 'none',
                            }}
                        >
                            Boards
                        </Typography>
                    </Link>
                </Toolbar>
            </Container>
        </AppBar>
    );
};

export default TopBar;
