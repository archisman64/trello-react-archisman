import { useState, useEffect, useRef } from 'react';
import { styled } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Paper } from '@mui/material';

const FormContainer = styled(Paper)(({ theme }) => ({
    margin: theme.spacing(1),
    padding: theme.spacing(1),
    backgroundColor: theme.palette.grey[300],
    borderRadius: theme.shape.borderRadius,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginBottom: theme.spacing(1),
}));

const StyledTextField = styled(TextField)(({ theme }) => ({
    margin: theme.spacing(1),
}));

const StyledButton = styled(Button)(({ theme }) => ({
    marginTop: theme.spacing(1),
}));

const AddCardForm = ({ onAddCard }) => {
    const [showForm, setShowForm] = useState(false);
    const [cardName, setCardName] = useState('');
    const formRef = useRef(null);

    const handleAddCard = () => {
        if (cardName.trim()) {
            onAddCard(cardName);
            setCardName('');
            setShowForm(false); // Hide the form after adding the card
        } else if (showForm) {
            // Close the form if it's already open and the input field is empty
            setShowForm(false);
        }
    };

    const handleShowForm = (event) => {
        event.stopPropagation(); // Stop the propagation of the click event on the button
        setShowForm(true);
    };

    const handleOutsideClick = (event) => {
        if (formRef.current && !formRef.current.contains(event.target)) {
            setShowForm(false); // Hide the form when user clicks outside the form area
        }
    };

    useEffect(() => {
        document.addEventListener('click', handleOutsideClick);
        return () => {
            document.removeEventListener('click', handleOutsideClick);
        };
    }, []);

    return (
        <FormContainer ref={formRef}>
            {!showForm ? (
                <Typography variant="body1" onClick={handleShowForm} style={{ cursor: 'pointer', width: '100%' }}>
                    + Add a card
                </Typography>
            ) : (
                <>
                    <StyledTextField
                        variant="outlined"
                        label="Card Name"
                        value={cardName}
                        onChange={(e) => setCardName(e.target.value)}
                    />
                    <StyledButton variant="contained" color="primary" onClick={handleAddCard}>
                        Add Card
                    </StyledButton>
                </>
            )}
        </FormContainer>
    );
};

export default AddCardForm;
