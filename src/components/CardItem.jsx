import { useState, useReducer } from 'react';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import ChecklistModal from './ChecklistModal.jsx'; // Import the ChecklistModal component
import { getChecklistsForCard } from '../services/trelloApi.js';
// import { checklistsReducer } from '../reducers/checklistsReducer.js';
import { useSelector, useDispatch } from 'react-redux';
import {
    setChecklists
} from '../features/checklists/checklistsSlice.js';

const StyledCardItem = styled(Paper)(({ theme }) => ({
    margin: theme.spacing(1),
    padding: theme.spacing(1),
    backgroundColor: theme.palette.grey[100],
    borderRadius: theme.shape.borderRadius,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between', // Add this to align delete icon to the right
}));

const CardItem = ({ card, onDelete }) => {
    const dispatch = useDispatch();
    const checklistsState = useSelector(state => state.checklistsState);

    // const checklistsReducer = (state, action) => {
    //     switch (action.type) {
    //         case 'ADD_CHECKLIST':
    //             return { ...state, data: [...state.data, action.payload] };
    //         case 'DELETE_CHECKLIST':
    //             // console.log('to delte cheklist id:', action.payload);
    //             return { ...state, data: state.data.filter((list) => list.id !== action.payload) };
    //         // return state.filter((list) => list.id !== action.payload);
    //         case 'SET_CHECKLISTS':
    //             return { ...state, data: action.payload };
    //         // return action.payload;
    //         case 'TOGGLE_CARD_DELETE_CONFIRMATION':
    //             return { ...state, isDeleteConfirmationOpen: !state.isDeleteConfirmationOpen };
    //         case 'TOGGLE_CHECKLIST_MODAL_STATUS':
    //             return { ...state, isChecklistModalOpen: !state.isChecklistModalOpen };
    //         case 'TOGGLE_ADDING_CHECKLIST':
    //             return { ...state, isAddingChecklist: !state.isAddingChecklist };
    //         case 'SET_NEW_CHECKLIST_NAME':
    //             return { ...state, newChecklistName: action.payload };
    //         default:
    //             return state;
    //     }
    // };

    // const [checklistsState, dispatch] = useReducer(checklistsReducer,
    //     {
    //         data: [],
    //         isDeleteConfirmationOpen: false,
    //         isChecklistModalOpen: false,
    //         isAddingChecklist: false,
    //         newChecklistName: ''
    //     }
    // );


    const [isDeleteConfirmationOpen, setDeleteConfirmationOpen] = useState(false);
    const [isChecklistModalOpen, setChecklistModalOpen] = useState(false);
    // const [checklists, setChecklists] = useState([]); // State to store the checklists

    const handleDeleteClick = (event) => {
        event.stopPropagation();
        // dispatch(toggleCardDeleteConfirmation())
        // dispatch({ type: 'TOGGLE_CARD_DELETE_CONFIRMATION' });
        setDeleteConfirmationOpen(true);
    };

    const handleDeleteConfirmationClose = () => {
        // dispatch(toggleCardDeleteConfirmation())
        // dispatch({ type: 'TOGGLE_CARD_DELETE_CONFIRMATION' });
        setDeleteConfirmationOpen(false);
    };

    const handleDeleteConfirmationYes = () => {
        onDelete(card.id, card.idList);
        // dispatch(toggleCardDeleteConfirmation())
        // dispatch({ type: 'TOGGLE_CARD_DELETE_CONFIRMATION' });
        setDeleteConfirmationOpen(false);
    };

    const handleChecklistClick = async () => {
        try {
            // Call the API service to get the checklists for the card
            const checklistsData = await getChecklistsForCard(card.id);
            dispatch(setChecklists({ cardId: card.id, checklists: checklistsData }))
            // dispatch({ type: 'SET_CHECKLISTS', payload: checklistsData });
            // setChecklists(checklistsData);
            // dispatch(toggleChecklistModalStatus())
            // dispatch({ type: 'TOGGLE_CHECKLIST_MODAL_STATUS' });
            setChecklistModalOpen(true);
        } catch (error) {
            console.error('Error fetching checklists:', error);
        }
    };

    const handleCloseChecklistModal = () => {
        // dispatch(toggleChecklistModalStatus())
        // dispatch({ type: 'TOGGLE_CHECKLIST_MODAL_STATUS' });
        setChecklistModalOpen(false);
    };

    return (
        <>
            <StyledCardItem onClick={handleChecklistClick}>
                <Typography variant="body2">{card.name}</Typography>
                <IconButton onClick={handleDeleteClick}>
                    <DeleteIcon />
                </IconButton>
            </StyledCardItem>

            {/* ChecklistModal component */}
            <ChecklistModal
                onClose={handleCloseChecklistModal}
                card={card}
                isChecklistModalOpen={isChecklistModalOpen}
            />

            <Dialog open={isDeleteConfirmationOpen} onClose={handleDeleteConfirmationClose}>
                <DialogTitle>Confirm Delete</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Are you sure you want to delete this card?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDeleteConfirmationClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleDeleteConfirmationYes} color="primary">
                        Delete
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default CardItem;
