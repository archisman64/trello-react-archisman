import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';

const BoardCard = ({ board, onClick }) => {
    return (
        <Card sx={{ position: 'relative', marginBottom: 2, width: 200 }} onClick={onClick} style={{ cursor: 'pointer' }}>
            <CardMedia component="img" sx={{ height: 140 }} image={board.prefs.backgroundImage ? board.prefs.backgroundImage : 'https://coolbackgrounds.io/images/backgrounds/blue/blue-background-088FDC-8ecd0503.jpg'} alt="Board Background" />
            <div
                style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    background: 'rgba(0, 0, 0, 0.5)',
                    color: '#fff',
                    padding: '8px',
                    textAlign: 'center',
                }}
            >
                {board.name}
            </div>
        </Card>
    );
};

export default BoardCard;



