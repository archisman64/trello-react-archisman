// maps through all the lists for a board

import { createList, deleteList } from '../services/trelloApi';
import ListCard from './ListCard';
import AddListForm from './AddListForm';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { addList, deleteAList } from '../features/lists/listsSlice';

const BoardLists = ({ boardName }) => {
    const lists = useSelector(state => state.lists);
    const dispatch = useDispatch();
    const { boardId } = useParams();

    const handleAddList = async (listName) => {
        try {
            const newList = await createList(boardId, listName);
            dispatch(addList(newList));
            // dispatch({ type: 'ADD_LIST', payload: newList });
        } catch (error) {
            console.error('Error creating list:', error);
        }
    };

    const handleDeleteList = async (listId) => {
        try {
            // console.log('list id in handle delete list:', listId)
            await deleteList(listId);
            dispatch(deleteAList(listId));
            // dispatch({ type: 'DELETE_LIST', payload: listId });
        } catch (error) {
            console.log('error while deleting list:', error);

        }
    }

    return (
        <>
            <div style={{ display: 'flex', flexDirection: 'column' }}>
                <h2 style={{ width: 'auto' }}>{boardName}</h2>
                <div style={{ display: 'flex', overflowX: 'auto', marginTop: '1em' }}>
                    {lists.map((list) => (
                        < ListCard key={list.id} list={list} onDeleteList={handleDeleteList} />
                    ))}
                    <AddListForm onAddList={handleAddList} />
                </div>
            </div>
        </>
    );
};

export default BoardLists;

