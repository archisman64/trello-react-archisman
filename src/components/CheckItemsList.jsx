import { List, Divider, Button, TextField, Stack } from '@mui/material';
import { useEffect, useState } from 'react';
import { getCheckItemsForChecklist, addCheckItemToChecklist, deleteCheckItemFromChecklist, updateCheckItem } from '../services/trelloApi';
import CheckItem from './CheckItem';
// import { checkItemsReducer } from '../reducers/checkItemsReducer';
import { useSelector, useDispatch } from 'react-redux';
import {
    addCheckItem,
    setCheckItems,
    deleteCheckItem,
    toggleCheckedState
}
    from '../features/checkItems/checkItemsSlice';

const CheckItemsList = ({ checklistId, cardId }) => {
    const checkItemsState = useSelector(state => state.checkItemsState);
    const dispatch = useDispatch();

    const [showInput, setShowInput] = useState(false);
    const [newCheckItemName, setNewCheckItemName] = useState('');
    // const [checkItems, setCheckItems] = useState([]);

    // const checkItemsReducer = (state, action) => {
    //     switch (action.type) {
    //         case 'ADD_CHECKITEM':
    //             return { ...state, data: [...state.data, action.payload] };
    //         case 'DELETE_CHECKITEM':
    //             return { ...state, data: state.data.filter((list) => list.id !== action.payload) };
    //         case 'TOGGLE_CHECKED_STATE':
    //             return {
    //                 ...state,
    //                 data: state.data.map((item) => item.id === action.payload.checkItemId ? { ...item, state: action.payload.newState } : item)
    //             }
    //         case 'SET_CHECKITEMS':
    //             return { ...state, data: action.payload };
    //         case 'TOGGLE_SHOW_INPUT':
    //             return { ...state, showInput: !state.showInput };
    //         case 'SET_CHECKITEM_NAME':
    //             return { ...state, newCheckItemName: action.payload };
    //         default:
    //             return state;
    //     }
    // };

    // const [checkItemsState, dispatch] = useReducer(
    //     checkItemsReducer,
    //     {
    //         data: [],
    //         showInput: false,
    //         newCheckItemName: ''
    //     }
    // );

    useEffect(() => {
        const fetchCheckItems = async () => {
            try {
                // Call the API service to get the check items for the checklist
                const checkItemsData = await getCheckItemsForChecklist(checklistId);
                dispatch(setCheckItems({ checklistId: checklistId, checkItems: checkItemsData }));
                // dispatch({ type: 'SET_CHECKITEMS', payload: checkItemsData });
                // setCheckItems(checkItemsData);
            } catch (error) {
                console.error('Error fetching check items:', error);
            }
        };

        fetchCheckItems();
    }, [checklistId]);

    const handleAddCheckItem = async () => {
        if (newCheckItemName.trim()) {
            try {
                // Make API call to add the new check item
                // Replace 'addCheckItemToChecklist' with your actual API function to add check items
                const newCheckItem = await addCheckItemToChecklist(checklistId, newCheckItemName);
                dispatch(addCheckItem({ checklistId: checklistId, checkItem: newCheckItem }));
                // dispatch({ type: 'ADD_CHECKITEM', payload: newCheckItem });
                // setCheckItems((prevItems) => [...prevItems, newCheckItem]);
                // dispatch(toggleShowInput());
                // dispatch({ type: 'TOGGLE_SHOW_INPUT' });
                setShowInput(false);
                // dispatch(setCheckItemName(''))
                // dispatch({ type: 'SET_CHECKITEM_NAME', payload: '' });
                setNewCheckItemName('');
            } catch (error) {
                console.error('Error adding check item:', error);
            }
        }
    };

    const handleDeleteCheckItem = async (checkItemId) => {
        await deleteCheckItemFromChecklist(checklistId, checkItemId);
        dispatch(deleteCheckItem({ checklistID: checklistId, checkItemID: checkItemId }));
        // dispatch({ type: 'DELETE_CHECKITEM', payload: checkItemId });
        // setCheckItems((prevItems) => prevItems.filter((item) => item.id !== checkItemId));
    }

    const handleCheckChange = async (checkItemId, newState) => {
        // Update the state property in the CheckItemsList component
        // setCheckItems(prevItems =>
        //     prevItems.map(item =>
        //         item.id === checkItemId ? { ...item, state: newState } : item
        //     )
        // );
        // Call the API to update the state status
        await updateCheckItem(cardId, checkItemId, newState);

        dispatch(toggleCheckedState(
            {
                checklistID: checklistId,
                checkItemID: checkItemId,
                newState: newState
            }
        ))

        // dispatch({
        //     type: 'TOGGLE_CHECKED_STATE',
        //     payload: {
        //         checkItemId: checkItemId,
        //         newState: newState
        //     }
        // });

    };

    // Function to cancel adding a new check item
    const handleCancelAddCheckItem = () => {
        // dispatch(toggleShowInput());
        // dispatch({ type: 'TOGGLE_SHOW_INPUT' });
        setShowInput(false);
        // dispatch(setCheckItemName(''));
        // dispatch({ type: 'SET_CHECKITEM_NAME', payload: '' });
        setNewCheckItemName('');
    };

    // Function to show the input area for adding a new check item
    const handleShowInput = () => {
        // dispatch(toggleShowInput())
        // dispatch({ type: 'TOGGLE_SHOW_INPUT' });
        setShowInput(true);
    };

    return (
        <List>
            {checkItemsState[checklistId] && checkItemsState[checklistId].map((item, index) => (
                <div key={index}>
                    <CheckItem item={item} onDelete={handleDeleteCheckItem} onCheckChange={handleCheckChange} />
                    {index !== checkItemsState[checklistId].length - 1 && <Divider />}
                </div>
            ))}

            {/* Add Item button */}
            {!showInput ? (
                <Button variant="outlined" onClick={handleShowInput}>
                    Add Item
                </Button>
            ) : (
                // Input area for adding a new check item
                <Stack direction="row" alignItems="center">
                    <TextField
                        variant="outlined"
                        label="Item Name"
                        value={newCheckItemName}
                        onChange={(e) => setNewCheckItemName(e.target.value)}
                    />
                    <Button variant="contained" color="primary" onClick={handleAddCheckItem}>
                        Save
                    </Button>
                    <Button variant="contained" onClick={handleCancelAddCheckItem}>
                        Cancel
                    </Button>
                </Stack>
            )}
        </List>
    );
};

export default CheckItemsList;
