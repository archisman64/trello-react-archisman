import { Dialog, DialogContent, DialogTitle, List, ListItem, ListItemText, Divider, Button } from '@mui/material';
import { addChecklistToCard, deleteChecklistFromCard } from '../services/trelloApi';
import CheckItemsList from './CheckItemsList';
import { useSelector, useDispatch } from 'react-redux';
import {
    addChecklist,
    deleteAChecklist
} from '../features/checklists/checklistsSlice';
import { useState } from 'react';

const ChecklistModal = ({ onClose, card, isChecklistModalOpen }) => {
    const dispatch = useDispatch();
    const checklistsState = useSelector(state => state.checklistsState);
    const [isAddingChecklist, setAddingChecklist] = useState(false);
    const [newChecklistName, setNewChecklistName] = useState('');

    const handleAddChecklist = () => {
        // dispatch(toggleAddingChecklist());
        // dispatch({ type: 'TOGGLE_ADDING_CHECKLIST' });
        setAddingChecklist(true);
    };

    const handleAddChecklistCancel = () => {
        // dispatch(toggleAddingChecklist());
        // dispatch({ type: 'TOGGLE_ADDING_CHECKLIST' });
        setAddingChecklist(false);
        // dispatch(setNewChecklistName(''));
        // dispatch({ type: 'SET_NEW_CHECKLIST_NAME', payload: '' });
        setNewChecklistName('');
    };

    const handleAddChecklistSave = async () => {
        const newChecklist = await addChecklistToCard(card.id, newChecklistName);
        dispatch(addChecklist({ cardId: card.id, checklist: newChecklist }));
        // dispatch({ type: 'ADD_CHECKLIST', payload: newChecklist });
        // setChecklists((prevChecklists) => [...prevChecklists, newChecklist]);
        handleAddChecklistCancel();
    };

    const handleDeleteChecklist = async (checklistId) => {
        // console.log('checklis id to delete:', checklistId);
        await deleteChecklistFromCard(card.id, checklistId);
        dispatch(deleteAChecklist({ cardID: card.id, checklistID: checklistId }));
        // dispatch({ type: 'DELETE_CHECKLIST', payload: checklistId });
        // setChecklists((prevChecklists) => prevChecklists.filter((checklist) => checklist.id !== checklistId));
    };

    return (
        <Dialog open={isChecklistModalOpen} onClose={onClose}>
            <DialogTitle>Checklist for "{card.name}"</DialogTitle>
            <DialogContent>
                <List>
                    {checklistsState[card.id] && checklistsState[card.id].map((checklist) => (
                        <div key={checklist.id}>
                            <ListItem>
                                <ListItemText primary={checklist.name} />
                                <Button onClick={() => handleDeleteChecklist(checklist.id)} color="secondary">
                                    Delete
                                </Button>
                            </ListItem>
                            <Divider />
                            {/* Display CheckItems under each Checklist */}
                            <CheckItemsList checklistId={checklist.id} cardId={card.id} />
                        </div>
                    ))}
                </List>
                {isAddingChecklist ? (
                    // Display the form or input field to add a new checklist
                    <>
                        <input
                            type="text"
                            value={newChecklistName}
                            onChange={(e) => setNewChecklistName(e.target.value)}
                            placeholder="Enter checklist name"
                        />
                        <Button onClick={handleAddChecklistSave} color="primary">
                            Save
                        </Button>
                        <Button onClick={handleAddChecklistCancel} color="primary">
                            Cancel
                        </Button>
                    </>
                ) : (
                    // Display the "Add Checklist" button
                    <Button onClick={handleAddChecklist} color="primary">
                        Add Checklist
                    </Button>
                )}
            </DialogContent>
        </Dialog>
    );
};

export default ChecklistModal;
