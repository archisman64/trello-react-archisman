import { styled } from '@mui/material/styles';
import Checkbox from '@mui/material/Checkbox';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
// import { updateCheckItem } from '../services/trelloApi';

const StyledCheckItem = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    marginBottom: theme.spacing(1),
}));

const CheckItem = ({ item, onDelete, onCheckChange }) => {
    // const [isChecked, setIsChecked] = useState(item.isChecked);

    const handleCheckChange = () => {
        onCheckChange(item.id, item.state === 'incomplete' ? 'complete' : 'incomplete');
        // You may need to update the isChecked value in your API after checking/unchecking
        // Example: updateChecklistItem(item.id, !isChecked);
    };

    const handleDeleteClick = () => {
        onDelete(item.id);
    };
    // console.log('item:', item)
    return (
        <StyledCheckItem>
            <Checkbox checked={item.state === 'complete' ? true : false} onChange={handleCheckChange} />
            <Typography variant="body2">{item.name}</Typography>
            <IconButton onClick={handleDeleteClick}>
                <DeleteIcon />
            </IconButton>
        </StyledCheckItem>
    );
};

export default CheckItem;
