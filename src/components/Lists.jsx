import { useEffect, useState, useReducer } from 'react';
import { useParams } from 'react-router-dom';
import { getListsForBoard } from '../services/trelloApi';
import BoardLists from './BoardLists';
import Loader from './Loader';
// import { listsReducer } from '../reducers/listsReducer';
import { useDispatch } from 'react-redux';
import { setLists } from '../features/lists/listsSlice';
import ErrorComponent from './ErrorComponent';

const Lists = ({ boards }) => {
    const dispatch = useDispatch();
    // const listsReducer = (state, action) => {
    //     switch (action.type) {
    //         case 'ADD_LIST':
    //             return [...state, action.payload];
    //         case 'DELETE_LIST':
    //             return state.filter((list) => list.id !== action.payload);
    //         case 'SET_LISTS':
    //             return action.payload;
    //         default:
    //             return state;
    //     }
    // };
    // const [lists, dispatch] = useReducer(listsReducer, []);


    const { boardId } = useParams();
    // const [lists, setLists] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    const currentBoard = boards.find((board) => board.id === boardId);

    useEffect(() => {
        const fetchLists = async () => {
            try {
                const listsData = await getListsForBoard(boardId);
                dispatch(setLists(listsData));
                // dispatch({ type: 'SET_LISTS', payload: listsData });
                setLoading(false);
            } catch (error) {
                setError(error);
                console.error('Error fetching lists:', error);
            }
        };

        fetchLists();
    }, [boardId]);

    if (error) {
        return <>{<ErrorComponent error={error} />}</>
    }

    return (
        <>
            {loading ? (
                <Loader />
            ) : (
                <div
                    style={{
                        height: '90vh',
                        display: 'flex',
                        alignItems: 'stretch',
                        background: `url(${currentBoard?.prefs.backgroundImage})`,
                        // backgroundColor: 'rgb(1,121,191)'
                        // backgroundSize: 'cover',
                        // backgroundPosition: 'center'
                    }}
                >
                    <BoardLists boardName={currentBoard?.name} />
                </div>
            )}
        </>
    );
};

export default Lists;
