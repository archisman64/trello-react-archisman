import { useState } from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import { createBoard } from '../services/trelloApi';

const CreateBoardModal = ({ open, onClose, onBoardCreated }) => {
    const [boardName, setBoardName] = useState('');

    const handleCreateBoard = async () => {
        try {
            const newBoard = await createBoard(boardName);
            onBoardCreated(newBoard); // Notify parent component that new board is created
            setBoardName('');
            onClose(); // Close the modal
        } catch (error) {
            console.error('Error creating board:', error);
        }
    };

    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>Create New Board</DialogTitle>
            <DialogContent>
                <DialogContentText>Please enter the name for the new board:</DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    label="Board Name"
                    fullWidth
                    value={boardName}
                    onChange={(e) => setBoardName(e.target.value)}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose}>Cancel</Button>
                <Button onClick={handleCreateBoard} variant="contained" color="primary">
                    Create
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default CreateBoardModal;
