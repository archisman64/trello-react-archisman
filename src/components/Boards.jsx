import { useState } from 'react';
import BoardCard from './BoardCard';
import CreateBoardModal from './CreateBoardModal';
import { Link } from 'react-router-dom';

const BoardsComponent = ({ boards, setBoards }) => {
    // const [boards, setBoards] = useState([]);
    const [isModalOpen, setIsModalOpen] = useState(false);

    const createBoardObj = {
        'name': 'Create new board',
        'prefs': {
            'backgroundImage': 'https://img.freepik.com/premium-photo/gray-abstract-wall-display-product_36835-2777.jpg'
        }
    }

    const handleModalOpen = () => {
        setIsModalOpen(true);
    };

    const handleCloseModal = () => {
        setIsModalOpen(false);
    };

    const handleBoardCreated = (newBoard) => {
        // Update the boards state with the new board
        setBoards((prevBoards) => [...prevBoards, newBoard]);
        setIsModalOpen(false);
    };

    // const handleBoardClick = async (boardId) => {
    //     try {
    //         const lists = await getListsForBoard(boardId);
    //         console.log('Lists for Board:', lists);
    //         // history.push(`/boards/${boardId}`);
    //     } catch (error) {
    //         console.log('Error fetching lists:', error);
    //     }
    // }

    return (
        <div>
            <div style={{ display: 'flex', flexWrap: 'wrap', columnGap: 12, paddingTop: 12 }}>
                <>
                    {boards.map((board) => (
                        <Link key={board.id} to={`boards/${board.id}`}>
                            <BoardCard board={board} />
                        </Link>
                    ))}
                </>
                <BoardCard board={createBoardObj} onClick={handleModalOpen}></BoardCard>
            </div>
            <CreateBoardModal open={isModalOpen} onClose={handleCloseModal} onBoardCreated={handleBoardCreated} />
        </div>
    );
};

export default BoardsComponent;
