import { useEffect } from 'react';
import { styled } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import { createCard, getCardsForList, deleteCard } from '../services/trelloApi';
import CardItem from './CardItem';
import AddCardForm from './AddCardForm';
import ListMenu from './ListMenu';
// import { cardsReducer } from '../reducers/cardsReducer';
import { useSelector, useDispatch } from 'react-redux';
import { addCard, deleteACard, setCards } from '../features/cards/cardsSlice';

const StyledCard = styled(Card)(({ theme }) => ({
    minWidth: 280,
    maxWidth: 320,
    backgroundColor: theme.palette.grey[300],
    marginRight: theme.spacing(2),
    padding: theme.spacing(1),
    borderRadius: theme.shape.borderRadius,
}));

const StyledCardContainer = styled('div')(({ theme }) => ({
    height: 'auto', // Set the height to 'auto' to adjust based on content
    display: 'flex',
    flexDirection: 'column', // Align card items vertically
    marginBottom: theme.spacing(1),
    overflowY: 'auto', // Add vertical scroll
}));

const StyledCardHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: theme.spacing(1),
}));

const ListCard = ({ list, onDeleteList }) => {
    const dispatch = useDispatch();
    const cards = useSelector(state => state.cards);
    // console.log('cards:', cards)
    // const cardsReducer = (state, action) => {
    //     switch (action.type) {
    //         case 'ADD_CARD':
    //             return [...state, action.payload];
    //         case 'DELETE_CARD':
    //             return state.filter((card) => card.id !== action.payload);
    //         case 'SET_CARDS':
    //             return action.payload;
    //         default:
    //             return state;
    //     }
    // };
    // const [cards, dispatch] = useReducer(cardsReducer, []);

    // const [cards, setCards] = useState([]);

    useEffect(() => {
        const fetchCards = async () => {
            try {
                const cardsData = await getCardsForList(list.id);
                // console.log('cards data:', cardsData)

                dispatch(setCards({ listId: list.id, cards: cardsData }));
            } catch (error) {
                console.error('Error fetching cards:', error);
            }
        };
        fetchCards();
    }, [list.id]);

    const addCardObj = {
        'name': '+ Add a card'
    }

    const handleAddCard = async (cardName) => {
        try {
            const newCard = await createCard(list.id, cardName);
            // setCards([...cards, newCard]);
            dispatch(addCard({ listId: list.id, card: newCard }));
            // dispatch({ type: 'ADD_CARD', payload: newCard });
        } catch (error) {
            console.error('Eorror creating card:', error);
        }

    };

    const handleDeleteList = (listId) => {
        // console.log('list id to delete:', listId);
        onDeleteList(listId);
    }

    const handleDeleteCard = async (cardId, listId) => {
        try {
            // console.log('cardId:', cardId)
            await deleteCard(cardId);

            dispatch(deleteACard({ listID: listId, cardID: cardId }));
            // dispatch({ type: 'DELETE_CARD', payload: cardId });
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <StyledCard style={{ marginBottom: 'auto' }}>
            <StyledCardHeader>
                <Typography variant="subtitle1" sx={{ marginBottom: 1, fontWeight: 'bold' }}>
                    {list.name}
                </Typography>
                <ListMenu list={list} onDelete={handleDeleteList} />
            </StyledCardHeader>
            <StyledCardContainer>
                {cards[list.id] && cards[list.id].map((card) => (
                    <CardItem key={card.id} card={card} onDelete={handleDeleteCard} />
                ))}
                <AddCardForm card={addCardObj} onAddCard={handleAddCard} />
            </StyledCardContainer>
        </StyledCard>
    );
};

export default ListCard;

