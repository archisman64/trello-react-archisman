import { useState } from 'react';
import { styled } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { Card, Typography } from '@mui/material';

const StyledCard = styled(Card)(({ theme }) => ({
    minWidth: 280,
    maxWidth: 320,
    // backgroundColor: 'transparent', // Set background to transparent
    marginRight: theme.spacing(2),
    padding: theme.spacing(1),
    borderRadius: theme.shape.borderRadius,
    marginBottom: 'auto',
    background: 'rgb(151,187,222)'
}));

const StyledCardContainer = styled('div')(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column'
}));

const StyledCardHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-sart',
    justifyContent: 'space-between',
    marginBottom: theme.spacing(1)
}));

const StyledTextField = styled(TextField)(({ theme }) => ({
    margin: theme.spacing(1),
}));

const StyledButton = styled(Button)(({ theme }) => ({
    marginTop: theme.spacing(1),
}));

const AddListForm = ({ onAddList }) => {
    const [listName, setListName] = useState('');
    const [showInput, setShowInput] = useState(false);

    const handleAddList = () => {
        if (listName.trim()) {
            onAddList(listName); // passing to parent
            setListName('');
        }
        setShowInput(false);
    };

    const handleShowInput = () => {
        setShowInput(true);
    };

    return (
        <StyledCard>
            <StyledCardContainer>
                <StyledCardHeader>
                    {showInput ? (
                        <>
                            <StyledTextField
                                variant="outlined"
                                label="List Name"
                                value={listName}
                                onChange={(e) => setListName(e.target.value)}
                                style={{ background: 'rgb(151,187,222)' }}
                            />
                            <StyledButton variant="contained" color="primary" onClick={handleAddList} >
                                Add List
                            </StyledButton>
                        </>
                    ) : (
                        <Typography variant='body-1' onClick={handleShowInput} style={{ cursor: 'pointer', width: '100%', color: 'white' }}>
                            + Add another list
                        </Typography>
                    )}
                </StyledCardHeader>
            </StyledCardContainer>
        </StyledCard>
    );
};

export default AddListForm;
