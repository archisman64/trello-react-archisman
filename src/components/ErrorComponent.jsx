const ErrorComponent = ({ error }) => {
    if (error.statusCode && error.statusCode === 404) {
        return (
            <>
                <h2>{error.statusCode}</h2>
                <p>Resource not found. Please check the URL.</p>
            </>
        )

    } else if (error.statusCode && error.statusCode === 500) {
        return (
            <>
                <h2>{error.statusCode}</h2>
                <p>An internal server error occurred. Please try again later.</p>
            </>
        )
    } else {
        return (
            <>
                <p>An error occurred:</p>
                <p>{error.message}</p>
            </>
        );
    }
};

export default ErrorComponent;
