import { configureStore } from "@reduxjs/toolkit";


import checklistsSlice from "./features/checklists/checklistsSlice";
import cardsSlice from "./features/cards/cardsSlice";
import checkItemsSlice from "./features/checkItems/checkItemsSlice";
import listsSlice from "./features/lists/listsSlice";

const store = configureStore({
    reducer: {
        cards: cardsSlice,
        checkItemsState: checkItemsSlice,
        checklistsState: checklistsSlice,
        lists: listsSlice
    }
});

export default store;