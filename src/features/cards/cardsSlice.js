import { createSlice } from "@reduxjs/toolkit";

const cardsSlice = createSlice({
    name: 'cards',
    initialState: {},
    reducers: {
        addCard: (state, action) => {
            const { listId, card } = action.payload;
            state[listId].push(card);
        },
        deleteACard: (state, action) => {
            const { listID, cardID } = action.payload;
            state[listID] = state[listID].filter(card => card.id !== cardID);
        },
        setCards: (state, action) => {
            const { listId, cards } = action.payload;
            state[listId] = cards;
        },
    },
});

export const { addCard, deleteACard, setCards } = cardsSlice.actions;

export default cardsSlice.reducer;