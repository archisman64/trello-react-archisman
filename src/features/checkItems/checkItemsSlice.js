import { createSlice } from '@reduxjs/toolkit';

const checkItemsSlice = createSlice({
    name: 'checkItems',
    initialState: {},
    reducers: {
        addCheckItem: (state, action) => {
            const { checklistId, checkItem } = action.payload;
            state[checklistId].push(checkItem);
        },
        deleteCheckItem: (state, action) => {
            const { checklistID, checkItemID } = action.payload;
            state[checklistID] = state[checklistID].filter(checkItem => checkItem.id !== checkItemID);
        },
        setCheckItems: (state, action) => {
            const { checklistId, checkItems } = action.payload;
            state[checklistId] = checkItems;
        },
        toggleCheckedState: (state, action) => {
            const { checklistID, checkItemID, newState } = action.payload;
            const requiredCheckItem = state[checklistID].find(checkItem => checkItem.id === checkItemID);
            if (requiredCheckItem.state !== newState) {
                requiredCheckItem.state = newState;
            }
        }
    },
});

export const {
    addCheckItem,
    deleteCheckItem,
    setCheckItems,
    toggleCheckedState
} = checkItemsSlice.actions;

export default checkItemsSlice.reducer;
