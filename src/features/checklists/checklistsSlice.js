import { createSlice } from "@reduxjs/toolkit";

const checklistsSlice = createSlice({
    name: 'checklistsState',
    initialState: {},
    reducers: {
        addChecklist: (state, action) => {
            const { cardId, checklist } = action.payload;
            state[cardId].push(checklist);
        },
        deleteAChecklist: (state, action) => {
            const { cardID, checklistID } = action.payload;
            state[cardID] = state[cardID].filter(checklist => checklist.id !== checklistID);
        },
        setChecklists: (state, action) => {
            const { cardId, checklists } = action.payload;
            state[cardId] = checklists;
        },
    },
});

export const {
    addChecklist,
    deleteAChecklist,
    setChecklists,
} = checklistsSlice.actions;

export default checklistsSlice.reducer;