import { createSlice } from '@reduxjs/toolkit';

const listsSlice = createSlice({
    name: 'lists',
    initialState: [],
    reducers: {
        addList: (state, action) => [...state, action.payload],
        deleteAList: (state, action) => state.filter(list => list.id !== action.payload),
        setLists: (_, action) => action.payload,
    },
});

export const { addList, deleteAList, setLists } = listsSlice.actions;

export default listsSlice.reducer;
